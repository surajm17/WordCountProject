package com.mindgate.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class WordCountTest {

	WordCount sut=new WordCount();
	
	@Test(expected=FileNotFounded.class)
	public void should_throw_exception_when_file_not_found(){
		sut.getFile("c:\\Users\\Mindgates\\text.txt");
	}
	
	@Test
	public void countWord(){
		int i= sut.getCountingWord("Hello World");
		System.out.println(i);
		assertEquals(i,2);
		
	}
}
