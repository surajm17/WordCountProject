package com.mindgate.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;

import org.junit.Test;

public class Char_Count_Test 
{
 Char_Count sut = new Char_Count();

 @Test	 
 public void find_file()
	 {
		 File f = sut.getFile("D:\\user1\\count_char_words\\" +
		 		"src\\main\\resources\\Test.txt");
		 assertNotNull(f);
		 assertEquals("Test.txt",f.getName());
	 }
	 
	 @Test(expected = File_Not_Found_Exception.class)
	 public void file_not_found()
	 {
		 File f = sut.getFile("D:\\user2\\Test.txt");
	 }
}